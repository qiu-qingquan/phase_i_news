package com.nx.arch.transactionmsg;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;

import com.alibaba.rocketmq.client.producer.DefaultMQProducer;
import com.nx.arch.transactionmsg.MsgProcessor;
import com.nx.arch.transactionmsg.MsgStorage;
import com.nx.arch.transactionmsg.model.Msg;
import com.nx.arch.transactionmsg.model.MsgInfo;
import com.nx.arch.transactionmsg.utils.Config;

@RunWith(MockitoJUnitRunner.class)
public class MsProcessorTest {

    @Mock
    private DefaultMQProducer producer;
    @Mock
    private MsgStorage msgStorage;
    
    private MsgProcessor msgProcessor;
    @Before
    public void setup(){
        msgProcessor = new MsgProcessor(producer, msgStorage);
        msgProcessor.init(new Config());
    }
    @Test
    public void hello() throws InterruptedException, SQLException{
        System.out.println("hello");
        Msg msg = new Msg(1L,"helloworldJDBCUrl"); 
        msgProcessor.putMsg(msg);
        Thread.sleep(10);
        MsgInfo msgInfo = new MsgInfo();
        msgInfo.setContent("helloworldmock");
        msgInfo.setId(msg.getId());
        msgInfo.setTopic("hellomock");;
        Mockito.when(msgStorage.getMsgById(msg)).thenReturn(msgInfo);
       // when(producer.send(msg))
        // mock 发送失败，发送成功
        Thread.sleep(100000);
    }
}
