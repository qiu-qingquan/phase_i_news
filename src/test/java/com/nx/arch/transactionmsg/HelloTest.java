package com.nx.arch.transactionmsg;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.UniformReservoir;

public class HelloTest {
    private static final Logger log = LoggerFactory.getLogger(HelloTest.class);
    
    private static String tableName = "mq_messages";
    
    private static String insertSQL = "insert into %s(content,topic,tag,status) values(?,?,?,?) ";
    
    /**
     * 10分钟
     */
    static int minuteTimeDiff = 1000 * 60 * 10;
    
    @Test
    public void hello()
        throws InterruptedException {
        String header = String.format("{\"topic\":\"%s\",\"tag\":\"%s\",\"id\":\"%s\",\"createTime\":\"%s\"}", "hello_topic", null, 123432, System.currentTimeMillis());
        System.out.println(header);
        Date date = new Date(System.currentTimeMillis() - 4 * 24 * 60 * 60 * 1000);
        System.out.println(date);
        int size = 100000;
        UniformReservoir reservoir = new UniformReservoir(size);
        // Timer cost = new Timer(reservoir);
        // cost.get
        Histogram histogram = new Histogram(reservoir);
        for (int i = 0; i < size; i++) {
            histogram.update(i);
            if (i == size - 1) {
                System.out.println(i);
            }
        }
        Thread.sleep(10);
        System.out.println(histogram.getCount());
        System.out.println(histogram.getSnapshot().getMedian());
        System.out.println(histogram.getSnapshot().getMax());
    }
    
    @Test
    public void testHashMap() {
        HashMap<String, Object> map = new HashMap<String, Object>(10);
        System.out.println(map.get("hello"));
        map.put("hello", null);
        System.out.println(map.get("hello"));
        insertSQL = String.format(insertSQL, tableName);
        System.out.println(insertSQL);
        String hello = "helloworld";
        System.out.println(hello.equals(null));
        long time = System.currentTimeMillis() - minuteTimeDiff;
        Timestamp timestamp = new Timestamp(time);
        System.out.println(timestamp);
    }
    
    @Test
    public void testDelay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 1);
        calendar.set(Calendar.MILLISECOND, 0);
        long initialDelay = calendar.getTimeInMillis() - System.currentTimeMillis();
        log.info("delay" + initialDelay);
        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(2);
        scheduledExecutor.scheduleAtFixedRate(new Runnable() {
            
            @Override
            public void run() {
                // TODO Auto-generated method stub
                log.info("start");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
            }
        }, initialDelay, 10 * 1000, TimeUnit.MILLISECONDS);
        try {
            Thread.sleep(1000000000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
