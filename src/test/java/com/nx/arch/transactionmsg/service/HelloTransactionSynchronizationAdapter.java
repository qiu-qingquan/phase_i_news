package com.nx.arch.transactionmsg.service;

import org.springframework.transaction.support.TransactionSynchronizationAdapter;

public class HelloTransactionSynchronizationAdapter extends TransactionSynchronizationAdapter{

    private Long msgid;
    public HelloTransactionSynchronizationAdapter(Long id){
        this.msgid = id;
    }
    @Override
    public void afterCommit() {
        System.out.println(this.msgid +" have commit");
    }
}
